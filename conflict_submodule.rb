#!/usr/bin/ruby
#encoding: utf-8
Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

require 'net/http'
require 'json'
require 'fileutils'
require 'uri'

#Локальное название внешнего сервера
$name_external_repo = "origin"

#Файлы в которых можно решить конфликты в 'мою' сторону
$conflict_files = "5ka-modules-ios,5ka-frameworks-ios,Configs/RootOptions.yml"

#Путь к проекту
$work_directory = "/Users/artemkulagin/Desktop/WorkLuxoft/Elegion/5ka-ios/5ka-ios"

#Аргумент нужен если в качестве аргумента используем МР. Генерируем каждый для себя в User Settings -> Access Tokens
$access_token = "glpat-hGzqH8Jfrx4EBXseYjoe"

class MR
    
    def getBranchesFromMR(mr_url)
        uri = URI.parse(mr_url)
        idMR = mr_url.split("/").last
        id = getIdProjectFromMR(mr_url)
        str = "https://#{uri.host}/api/v4/projects/#{id}/merge_requests/#{idMR}?access_token=#{$access_token}"
        p "Делаем запрос #{str}"
        uri = URI(str)
        res = Net::HTTP.get_response(uri)
        body = res.body
        json = JSON.parse(body)
        your_branch = json['source_branch']
        target_branch = json['target_branch']
        p "в ответе нашли:"
        p "     название нашей ветки #{your_branch}"
        p "     название целевой ветки #{target_branch}"
        return your_branch, target_branch
    end
    
    def getIdProjectFromMR(mr_url)
        uri = URI.parse(mr_url)
        name = mr_url.split("/")[4]
        p "имя проекта: #{name}"
        
        str = "https://#{uri.host}/api/v4/projects?access_token=#{$access_token}&search=#{name}"
        p "Делаем запрос #{str}"
        uri = URI(str)
        res = Net::HTTP.get_response(uri)
        body = res.body
        json = JSON.parse(body)
        id = json.first['id']
        p "Найден id проекта"
        return id
    end
end

class Branch
    
    def check_exist_variable_branch(name, branch)
        if branch.nil?
            puts "Не найдена " + name
            printInstruction
            return false
        else
            puts name + ": " + branch
            return true
        end
    end
    
    def checkout(branch)
        if !(system("git checkout -b '#{branch}' '#$name_external_repo/#{branch}'"))
            if !(system("git checkout '#{branch}'"))
                puts "Ошибка чекаута ветки #{branch}"
                printInstruction
                return false
            end
        end
        
        system("git pull")
        return true
    end
    
    def getBranches
        #Это путь к мр?
        arg0 = ARGV[0]
        arg1 = ARGV[1]
        
        if arg0.nil?
            p "Аргументы не найдены"
            printInstruction
            exit
        end
        
        uri = URI.parse(arg0)
        if uri.kind_of?(URI::HTTP) or uri.kind_of?(URI::HTTPS)
            p "Это мр, делаем запрос на сервер для получения веток"
            return MR.new.getBranchesFromMR(arg0)
        else
            p "Это не мр, считаем что в аргументах пришли названия веток"
            return arg0, arg1
        end
    end
end

def printInstruction
    p "ИНСТРУКЦИЯ."
    p "Скрипт автоматически разрешает конфликты в 'вашу сторону' в файлах:"
    $conflict_files.split(",").each do |line|
        p "   #{line}"
    end
    p "Для работы со скриптом:"
    p "1. Переходим в ветку с проектом."
    p "2. Запускаем скрипт в формате:\n"
    p "   conflict_submodule.rb loyalty/task/DGPT-XXXXX_task loyalty/story/DGPT-XXXXX_story"
    p "   где"
    p "     - loyalty/task/DGPT-XXXXX_task - ветка с вашими свежими изменениями (Ваша ветка)"
    p "     - loyalty/story/DGPT-XXXXX_story - ветка куда вливаете ваши изменения (Целевая ветка)\n\n"
    p "   или"
    p "   conflict_submodule.rb https://gitlab.e-legion.com/5ka/5ka-ios/-/merge_requests/4522"
    p "   где аргументом служит ссылка на мр"
end

class Main
    
    def parseOutputMerge(str)
        items = []
        str.each_line do |line|
            if line.start_with?('CONFLICT')
                last = line.split(" ").last
                items.append(last)
            end
        end
        return items
    end
    
    def canMerge(items)
        value = true
        items.each do |str|
            result = $conflict_files.include?(str)
            if !(result)
                p "Этот файл можно смержить только вручную: #{str}"
                value = false
            end
        end
        return value
    end
    
    def start
        
        p "Переходим в рабочую ветку #{$work_directory}"
        Dir.chdir($work_directory)
        
        your_branch, target_branch = Branch.new.getBranches
        branch = Branch.new
        #Проверяем пришла ли переменная для вашей ветки
        if !(branch.check_exist_variable_branch("ваша ветка", your_branch))
            exit
        end
        
        #Проверяем пришла ли переменная для целевой ветки
        if !(branch.check_exist_variable_branch("целевая ветка", target_branch))
            exit
        end
   
        #Делаем чекаут для целевой ветки
        if !branch.checkout(target_branch)
            exit
        end
        
        #Делаем чекаут для вашей ветки
        if !branch.checkout(your_branch)
            exit
        end
        
        #При переключении веток могут появится изменения в коммитах сабмодулях
        #Эта команда убирает изменения. (Возможно убирает еще что то, нужно тестить)
        system("git submodule update --init")
        
        #Попытка мержа целевой ветки в исходную
        output = `git merge --no-ff '#{target_branch}'`
        
        #Получаем все файлы в которых есть конфликты
        allFiles = parseOutputMerge(output)
        p "Файлы с конфликтами:"
        allFiles.each do |str|
            p "    #{str}"
        end
        
        if allFiles.count == 0
           p "Нет файлов с конфликтами, прекращаем работу скрипта"
           exit
        end
        
        if canMerge(allFiles)
            p "можно мержить конфликты в 'мою' сторону"
            
            allFiles.each do |str|
                p "Решаем конфликты в 'мою' сторону в файле #{str}"
                system("git checkout --ours #{str}")
            end
            
            allFiles.each do |str|
                p "Добавляем файл #{str} в индекс"
                system("git add -f -- #{str}")
            end
            p "создаем коммит"
            system("git commit -am 'Merge branch '#{target_branch}' into #{your_branch}'")
            p "пушим коммит"
            system("git push")
        else
            p "Отменяем мерж, так как есть файлы которые можно смержить только вручную"
            `git reset --hard HEAD`
            exit
        end
    end
end

Main.new.start
