#!/usr/bin/ruby
#encoding: utf-8
Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

require 'net/http'
require 'json'
require 'fileutils'
require 'uri'

#Локальное название внешнего сервера
$name_external_repo = "origin"

#Модули для которых нужно обновить ссылки
$submodules = ["5ka-frameworks-ios", "5ka-modules-ios"]

class Branch
    
    def check_exist_variable_branch(branch)
        if branch.nil?
            puts "Не найдена ветка"
            printInstruction
            return false
        else
            puts "Ваша ветка: " + branch
            return true
        end
    end
    
    def checkout(branch)
        if !(system("git checkout -b '#{branch}' '#$name_external_repo/#{branch}'"))
            if !(system("git checkout '#{branch}'"))
                puts "Ошибка чекаута ветки #{branch}"
                return false
            end
        end
        p "Получаем все изменения"
        system("git pull")
        return true
    end
end

class Submodule

    
end

def printInstruction
    p "ИНСТРУКЦИЯ."
    p "Скрипт обновляет ссылки для сабмодулей после мержа"
    p "Принцип действия: скрипт находит в сабмодулях одноименные ветки\nи получает последние изменения"
    p "После этого сохраняет изменения в монолите"
    p "Для работы со скриптом:"
    p "1. Переходим в ветку с проектом."
    p "2. Запускаем скрипт в формате:\n"
    p "   update_links_submodules.rb loyalty/story/DGPT-XXXXX_story"
    p "   где"
    p "     - loyalty/story/DGPT-XXXXX_story - ветка в которой нужно обновить ссылки"
end

class Main
    
    def parseOutputMerge()
        p "Получаем файлы которые не закоммичены"
        output = `git diff --name-only`
        return output.split("\n")
    end
    
    def canCommit(items)
        value = true
        items.each do |str|
            result = $submodules.include?(str)
            if !(result)
                p "Этот файл не является ссылкой на сабмодуль: #{str}"
                value = false
            end
        end
        return value
    end
    
    def start
        branch = ARGV[0]
        p "Проверяем пришла ли переменная для ветки монолита"
        if !(Branch.new.check_exist_variable_branch(branch))
            exit
        end
        
        p "Делаем чекаут для для ветки монолита"
        if !Branch.new.checkout(branch)
            exit
        end
        
        #При переключении веток могут появится изменения в коммитах сабмодулях
        #Эта команда убирает изменения. (Возможно убирает еще что то, нужно тестить)
        system("git submodule update --init")
        
        work_directory = Dir.pwd
        p "Рабочая директория монолита #{work_directory}"
        
        $submodules.each do |dir|
            p "Переходим в рабочую ветку #{dir}"
            Dir.chdir(work_directory + "/" + dir)
            p "Делаем чекаут в сабмодуле #{dir}"
            Branch.new.checkout(branch)
        end
        p "Переходим обратно в директорию монолита #{work_directory}"
        Dir.chdir(work_directory)
        
        allFiles = parseOutputMerge()
        p "Не закоммиченные файлы"
        allFiles.each do |str|
            p "    #{str}"
        end
        
        if allFiles.count == 0
           p "Нет файлов для коммита, прекращаем работу скрипта"
           exit
        end
        
        if canCommit(allFiles)
            allFiles.each do |str|
                p "Добавляем файл #{str} в индекс"
                system("git add -f -- #{str}")
            end
            p "Cоздаем коммит"
            system("git commit -am 'update links submodules'")
            p "Пушим коммит"
            system("git push")
        else
            p "Отменяем коммит, так как есть файлы которые не являются ссылками на сабмодуль"
            `git reset --hard HEAD`
            exit
        end
        
    end
end

Main.new.start
